package pl.wat.wcy.orr.springboot.rest.ztmservice.wojewodztwo;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class WojewodztwoNotFoundException extends RuntimeException {

	public WojewodztwoNotFoundException(String exception) {
		super(exception);
	}

}
