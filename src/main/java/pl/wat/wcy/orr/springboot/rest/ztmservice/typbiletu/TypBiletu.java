package pl.wat.wcy.orr.springboot.rest.ztmservice.typbiletu;

import pl.wat.wcy.orr.springboot.rest.ztmservice.bilet.Bilet;

import javax.persistence.*;
import java.util.List;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class TypBiletu {
	@Id
	@GeneratedValue
	@Column(name="typid")
	private Long typId;

	@Column(name="nazwatyp")
	private String nazwaTyp;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="typid")
	private List<Bilet> bilet;

	public TypBiletu() {
		super();
	}

	public TypBiletu(Long typId, String nazwaTyp, List <Bilet> bilet){
		this.typId = typId;
		this.nazwaTyp = nazwaTyp;
		this.bilet = bilet;
	}

	public Long getTypId() {
		return typId;
	}

	public void setTypId(Long typId) {
		this.typId = typId;
	}

	public String getNazwaTyp() {
		return nazwaTyp;
	}

	public void setNazwaTyp(String nazwaTyp) {
		this.nazwaTyp = nazwaTyp;
	}

	public List <Bilet> getBilet() {
		return bilet;
	}

	public void setBilet(List <Bilet> bilet) {
		this.bilet = bilet;
	}
}
