package pl.wat.wcy.orr.springboot.rest.ztmservice.bilet;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class BiletNotFoundException extends RuntimeException {

	public BiletNotFoundException(String exception) {
		super(exception);
	}

}
