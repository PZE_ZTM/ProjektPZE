package pl.wat.wcy.orr.springboot.rest.ztmservice.typmandatu;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypMandatuRepository extends JpaRepository<TypMandatu, Long>{

}
