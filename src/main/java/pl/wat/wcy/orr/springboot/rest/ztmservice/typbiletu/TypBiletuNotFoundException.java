package pl.wat.wcy.orr.springboot.rest.ztmservice.typbiletu;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class TypBiletuNotFoundException extends RuntimeException {

	public TypBiletuNotFoundException(String exception) {
		super(exception);
	}

}
