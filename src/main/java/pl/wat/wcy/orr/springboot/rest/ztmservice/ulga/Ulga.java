package pl.wat.wcy.orr.springboot.rest.ztmservice.ulga;

import pl.wat.wcy.orr.springboot.rest.ztmservice.bilet.Bilet;
import pl.wat.wcy.orr.springboot.rest.ztmservice.kartamiejska.KartaMiejska;

import javax.persistence.*;
import java.util.List;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class Ulga {
	@Id
	@GeneratedValue
	@Column(name="ulgaid")
	private Long ulgaId;

	@Column(name="rodzajid")
	private Long rodzajId;

	@Column(name="nazwa")
	private String nazwa;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="ulgaid")
	private List<Bilet> bilet;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="ulgaid")
	private List<KartaMiejska> kartaMiejska;

	public Ulga() {
		super();
	}

	public Ulga(Long ulgaId, String nazwa, Long rodzajId, List <Bilet> bilet, List <KartaMiejska> kartaMiejska){
		this.ulgaId = ulgaId;
		this.nazwa=nazwa;
		this.rodzajId = rodzajId;
		this.bilet = bilet;
		this.kartaMiejska = kartaMiejska;
	}

	public Long getUlgaId() {
		return ulgaId;
	}

	public void setUlgaId(Long ulgaId) {
		this.ulgaId = ulgaId;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Long getRodzajId() {
		return rodzajId;
	}

	public void setRodzajId(Long rodzajId) {
		this.rodzajId = rodzajId;
	}

	public List <Bilet> getBilet() {
		return bilet;
	}

	public void setBilet(List <Bilet> bilet) {
		this.bilet = bilet;
	}

	public List <KartaMiejska> getKartaMiejska() {
		return kartaMiejska;
	}

	public void setKartaMiejska(List <KartaMiejska> kartaMiejska) {
		this.kartaMiejska = kartaMiejska;
	}
}
