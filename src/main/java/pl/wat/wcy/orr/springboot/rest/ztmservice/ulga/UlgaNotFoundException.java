package pl.wat.wcy.orr.springboot.rest.ztmservice.ulga;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class UlgaNotFoundException extends RuntimeException {

	public UlgaNotFoundException(String exception) {
		super(exception);
	}

}
