package pl.wat.wcy.orr.springboot.rest.ztmservice.miasto;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class MiastoNotFoundException extends RuntimeException {

	public MiastoNotFoundException(String exception) {
		super(exception);
	}

}
