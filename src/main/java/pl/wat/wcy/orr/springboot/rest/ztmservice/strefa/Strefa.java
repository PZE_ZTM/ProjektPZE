package pl.wat.wcy.orr.springboot.rest.ztmservice.strefa;

import pl.wat.wcy.orr.springboot.rest.ztmservice.bilet.Bilet;
import pl.wat.wcy.orr.springboot.rest.ztmservice.kartamiejska.KartaMiejska;

import javax.persistence.*;
import java.util.List;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class Strefa {
	@Id
	@GeneratedValue
	@Column(name="strefaId")
	private Long strefaId;

	@Column(name="nazwastrefa")
	private String nazwaStrefa;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="strefaId")
	private List<Bilet> bilet;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="strefaId")
	private List<KartaMiejska> kartaMiejska;


	public Strefa() {
		super();
	}

	public Strefa(Long strefaId, String nazwaStrefa, List <Bilet> bilet, List <KartaMiejska> kartaMiejska){
		this.strefaId = strefaId;
		this.nazwaStrefa = nazwaStrefa;

		this.bilet = bilet;
		this.kartaMiejska = kartaMiejska;
	}

	public Long getStrefaId() {
		return strefaId;
	}

	public void setStrefaId(Long strefaId) {
		this.strefaId = strefaId;
	}

	public String getNazwaStrefa() {
		return nazwaStrefa;
	}

	public void setNazwaStrefa(String nazwaStrefa) {
		this.nazwaStrefa = nazwaStrefa;
	}

	public List <Bilet> getBilet() {
		return bilet;
	}

	public void setBilet(List <Bilet> bilet) {
		this.bilet = bilet;
	}

	public List <KartaMiejska> getKartaMiejska() {
		return kartaMiejska;
	}

	public void setKartaMiejska(List <KartaMiejska> kartaMiejska) {
		this.kartaMiejska = kartaMiejska;
	}
}
