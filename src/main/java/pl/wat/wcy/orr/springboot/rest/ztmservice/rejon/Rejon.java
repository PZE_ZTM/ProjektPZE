package pl.wat.wcy.orr.springboot.rest.ztmservice.rejon;

import pl.wat.wcy.orr.springboot.rest.ztmservice.kontroler.Kontroler;
import pl.wat.wcy.orr.springboot.rest.ztmservice.wojewodztwo.Wojewodztwo;

import javax.persistence.*;
import java.util.List;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class Rejon {
	@Id
	@GeneratedValue
	@Column(name="rejonid")
	private Long rejonId;

	@Column(name="nazwarejonu")
	private String nazwaRejonu;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="rejonId")
	private List<Kontroler> kontroler;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="wojewodztwoid")
	private Wojewodztwo wojewodztwo;

	public Rejon() {
		super();
	}

	public Rejon(Long rejonId, String nazwaRejonu, List <Kontroler> kontroler, Wojewodztwo wojewodztwo){
		this.rejonId = rejonId;
		this.nazwaRejonu = nazwaRejonu;
		this.kontroler = kontroler;
		this.wojewodztwo = wojewodztwo;
	}

	public Long getRejonId() {
		return rejonId;
	}

	public void setRejonId(Long rejonId) {
		this.rejonId = rejonId;
	}

	public String getNazwaRejonu() {
		return nazwaRejonu;
	}

	public void setNazwaRejonu(String nazwaRejonu) {
		this.nazwaRejonu = nazwaRejonu;
	}

	public Wojewodztwo getWojewodztwo() {
		return wojewodztwo;
	}

	public void setWojewodztwo(Wojewodztwo wojewodztwo) {
		this.wojewodztwo = wojewodztwo;
	}

	public List <Kontroler> getKontroler() {
		return kontroler;
	}

	public void setKontroler(List <Kontroler> kontroler) {
		this.kontroler = kontroler;
	}
}
