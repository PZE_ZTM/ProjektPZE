package pl.wat.wcy.orr.springboot.rest.ztmservice.strefa;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StrefaRepository extends JpaRepository<Strefa, Long>{

}
