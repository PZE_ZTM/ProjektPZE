package pl.wat.wcy.orr.springboot.rest.ztmservice.kodpocztowy;

import pl.wat.wcy.orr.springboot.rest.ztmservice.adres.Adres;

import javax.persistence.*;
import java.util.List;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class KodPocztowy {
	@Id
	@GeneratedValue
	@Column(name="kodPocztowyId")
	private Long kodPocztowyId;

	@Column(name="nazwa")
	private String nazwa;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "kodPocztowyId")
	private List<Adres> adres;

	public KodPocztowy() {
		super();
	}

	public KodPocztowy(Long kodPocztowyId, String nazwa, List <Adres> adres){
		this.kodPocztowyId = kodPocztowyId;
		this.nazwa = nazwa;
		this.adres = adres;
	}

	public Long getKodPocztowyId() {
		return kodPocztowyId;
	}

	public void setKodPocztowyId(Long kodPocztowyId) {
		this.kodPocztowyId = kodPocztowyId;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public List <Adres> getAdres() {
		return adres;
	}

	public void setAdres(List <Adres> adres) {
		this.adres = adres;
	}
}
