package pl.wat.wcy.orr.springboot.rest.ztmservice.adres;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class AdresNotFoundException extends RuntimeException {

	public AdresNotFoundException(String exception) {
		super(exception);
	}

}
