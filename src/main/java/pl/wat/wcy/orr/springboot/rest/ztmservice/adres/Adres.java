package pl.wat.wcy.orr.springboot.rest.ztmservice.adres;

import pl.wat.wcy.orr.springboot.rest.ztmservice.dluznik.Dluznik;
import pl.wat.wcy.orr.springboot.rest.ztmservice.kodpocztowy.KodPocztowy;
import pl.wat.wcy.orr.springboot.rest.ztmservice.miasto.Miasto;

import javax.persistence.*;
import java.util.List;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class Adres {
	@Id
	@GeneratedValue
	@Column(name="adresId")
	private Long adresId;

	@Column(name="ulicaNr")
	private String ulicaNr;

	@Column(name="wojewodztwoId")
	private Long wojewodztwoId;

	@Column(name="ulica")
	private String ulica;

	@Column(name="mieszkanieNr")
	private String mieszkanieNr;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "adresId")
	private List<Dluznik> dluznik;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "kodPocztowyId")
	private KodPocztowy kodPocztowy;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "miastoId")
	private Miasto miasto;


	public Adres() {
		super();
	}

	public Adres(Long adresId, String ulicaNr, Long wojewodztwoId, String ulica, String mieszkanieNr, List <Dluznik> dluznik, KodPocztowy kodPocztowy, Miasto miasto){
		this.adresId = adresId;
		this.ulicaNr = ulicaNr;
		this.wojewodztwoId = wojewodztwoId;
		this.ulica = ulica;
		this.mieszkanieNr = mieszkanieNr;
		this.dluznik = dluznik;
		this.kodPocztowy = kodPocztowy;
		this.miasto = miasto;
	}

	public Long getAdresId() {
		return adresId;
	}

	public void setAdresId(Long adresId) {
		this.adresId = adresId;
	}

	public String getMieszkanieNr() {
		return mieszkanieNr;
	}

	public void setMieszkanieNr(String mieszkanieNr) {
		this.mieszkanieNr = mieszkanieNr;
	}

	public String getUlicaNr() {
		return ulicaNr;
	}

	public void setUlicaNr(String ulicaNr) {
		this.ulicaNr = ulicaNr;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public Long getWojewodztwoId() {
		return wojewodztwoId;
	}

	public void setWojewodztwoId(Long wojewodztwoId) {
		this.wojewodztwoId = wojewodztwoId;
	}

	public KodPocztowy getKodPocztowy() {
		return kodPocztowy;
	}

	public void setKodPocztowy(KodPocztowy kodPocztowy) {
		this.kodPocztowy = kodPocztowy;
	}

	public Miasto getMiasto() {
		return miasto;
	}

	public void setMiasto(Miasto miasto) {
		this.miasto = miasto;
	}

	public List <Dluznik> getDluznik() {
		return dluznik;
	}

	public void setDluznik(List <Dluznik> dluznik) {
		this.dluznik = dluznik;
	}
}
