package pl.wat.wcy.orr.springboot.rest.ztmservice.typkartamiejska;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class TypKartaMiejskaNotFoundException extends RuntimeException {

	public TypKartaMiejskaNotFoundException(String exception) {
		super(exception);
	}

}
