package pl.wat.wcy.orr.springboot.rest.ztmservice.kodpocztowy;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KodPocztowyRepository extends JpaRepository<KodPocztowy, Long>{

}
