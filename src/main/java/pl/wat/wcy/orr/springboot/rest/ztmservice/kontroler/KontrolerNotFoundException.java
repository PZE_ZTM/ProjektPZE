package pl.wat.wcy.orr.springboot.rest.ztmservice.kontroler;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class KontrolerNotFoundException extends RuntimeException {

	public KontrolerNotFoundException(String exception) {
		super(exception);
	}

}
