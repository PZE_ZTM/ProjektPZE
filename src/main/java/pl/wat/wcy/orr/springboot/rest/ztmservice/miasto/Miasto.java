package pl.wat.wcy.orr.springboot.rest.ztmservice.miasto;

import pl.wat.wcy.orr.springboot.rest.ztmservice.adres.Adres;

import javax.persistence.*;
import java.util.List;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class Miasto {
	@Id
	@GeneratedValue
	@Column(name="miastoId")
	private Long miastoId;

	@Column(name="nazwa")
	private String nazwa;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="miastoId")
	private List<Adres> adres;

	public Miasto() {
		super();
	}

	public Miasto(Long miastoId, String nazwa, List <Adres> adres){
		this.miastoId = miastoId;
		this.nazwa = nazwa;
		this.adres = adres;
	}

	public Long getMiastoId() {
		return miastoId;
	}

	public void setMiastoId(Long miastoId) {
		this.miastoId = miastoId;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public List <Adres> getAdres() {
		return adres;
	}

	public void setAdres(List <Adres> adres) {
		this.adres = adres;
	}
}
