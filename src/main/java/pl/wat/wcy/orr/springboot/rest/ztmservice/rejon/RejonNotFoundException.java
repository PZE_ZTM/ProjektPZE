package pl.wat.wcy.orr.springboot.rest.ztmservice.rejon;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class RejonNotFoundException extends RuntimeException {

	public RejonNotFoundException(String exception) {
		super(exception);
	}

}
