package pl.wat.wcy.orr.springboot.rest.ztmservice.kodpocztowy;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class KodPocztowyNotFoundException extends RuntimeException {

	public KodPocztowyNotFoundException(String exception) {
		super(exception);
	}

}
