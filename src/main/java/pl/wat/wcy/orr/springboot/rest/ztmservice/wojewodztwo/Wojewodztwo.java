package pl.wat.wcy.orr.springboot.rest.ztmservice.wojewodztwo;

import pl.wat.wcy.orr.springboot.rest.ztmservice.rejon.Rejon;

import javax.persistence.*;
import java.util.List;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class Wojewodztwo {
	@Id
	@GeneratedValue
	@Column(name="wojewodztwoid")
	private Long wojewodztwoId;

	@Column(name="nazwa")
	private String nazwa;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="wojewodztwoid")
	private  List<Rejon>  rejon;


	public Wojewodztwo() {
		super();
	}

	public Wojewodztwo(Long wojewodztwoId, String nazwa,  List <Rejon> rejon){
		this.wojewodztwoId = wojewodztwoId;
		this.nazwa=nazwa;
		this.rejon = rejon;
	}

	public Long getWojewodztwoId() {
		return wojewodztwoId;
	}

	public void setWojewodztwoId(Long wojewodztwoId) {
		this.wojewodztwoId = wojewodztwoId;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public List <Rejon> getRejon() {
		return rejon;
	}

	public void setRejon(List <Rejon> rejon) {
		this.rejon = rejon;
	}
}
