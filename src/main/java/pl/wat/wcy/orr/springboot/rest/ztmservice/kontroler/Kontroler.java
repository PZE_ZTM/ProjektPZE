package pl.wat.wcy.orr.springboot.rest.ztmservice.kontroler;

import pl.wat.wcy.orr.springboot.rest.ztmservice.mandat.Mandat;
import pl.wat.wcy.orr.springboot.rest.ztmservice.rejon.Rejon;

import javax.persistence.*;
import java.util.List;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class Kontroler {
	@Id
	@GeneratedValue
	@Column(name="kontrolerid")
	private Long kontrolerId;

	@Column(name="imie")
	private String	imie;

	@Column(name="nazwisko")
	private	String nazwisko;

	@Column(name="email")
	private	String email;

	@Column(name="haslo")
	private	String haslo;

	@Column(name="pesel")
	private String pesel;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rejonId")
	private Rejon rejon;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "kontrolerId")
	private List<Mandat> mandat;


	public Kontroler() {
		super();
	}

	public Kontroler(Long kontrolerId, String imie, String nazwisko, String email, String haslo, String pesel, Rejon rejon, List <Mandat> mandat){

		this.kontrolerId = kontrolerId;

		this.imie = imie;
		this.nazwisko = nazwisko;
		this.email = email;
		this.haslo = haslo;
		this.pesel = pesel;
		this.rejon = rejon;
		this.mandat = mandat;
	}

	public Long getKontrolerId() {
		return kontrolerId;
	}

	public void setKontrolerId(Long kontrolerId) {
		this.kontrolerId = kontrolerId;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHaslo() {
		return haslo;
	}

	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}


	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public List <Mandat> getMandat() {
		return mandat;
	}

	public void setMandat(List <Mandat> mandat) {
		this.mandat = mandat;
	}

	public Rejon getRejon() {
		return rejon;
	}

	public void setRejon(Rejon rejon) {
		this.rejon = rejon;
	}
}
