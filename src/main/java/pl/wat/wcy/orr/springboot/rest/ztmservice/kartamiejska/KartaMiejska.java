package pl.wat.wcy.orr.springboot.rest.ztmservice.kartamiejska;

import pl.wat.wcy.orr.springboot.rest.ztmservice.strefa.Strefa;
import pl.wat.wcy.orr.springboot.rest.ztmservice.typkartamiejska.TypKartaMiejska;
import pl.wat.wcy.orr.springboot.rest.ztmservice.ulga.Ulga;
import pl.wat.wcy.orr.springboot.rest.ztmservice.uzytkownik.Uzytkownik;

import javax.persistence.*;
import java.sql.Timestamp;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class KartaMiejska {
	@Id
	@GeneratedValue
	@Column(name="kartaMiejskaId")
	private Long kartaMiejskaId ;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "typkartamiejskaid")
	private TypKartaMiejska typKartaMiejska;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "strefaId")
	private Strefa strefa;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ulgaid")
	private Ulga ulga;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "uzytkownikid")
	private Uzytkownik uzytkownik;

	@Column(name="email")
	private String email ;

	@Column(name="waznyOd")
	private Timestamp waznyOd ;

	@Column(name="waznyDo")
	private Timestamp waznyDo ;

	@Column(name="aktywna")
	private Short aktywna ;


	public KartaMiejska() {
		super();
	}

	public KartaMiejska(Long kartaMiejskaId, TypKartaMiejska typKartaMiejska, Strefa strefa, Ulga ulga, Uzytkownik uzytkownik, String email, Timestamp waznyOd, Timestamp waznyDo, Short aktywna){

		this.kartaMiejskaId=kartaMiejskaId;
		this.typKartaMiejska = typKartaMiejska;
		this.strefa = strefa;
		this.ulga = ulga;
		this.uzytkownik = uzytkownik;
		this.email = email;
		this.waznyOd = waznyOd;
		this.waznyDo = waznyDo;
		this.aktywna = aktywna;
	}

	public Long getKartaMiejskaId() {
		return kartaMiejskaId;
	}

	public void setKartaMiejskaId(Long kartaMiejskaId) {
		this.kartaMiejskaId = kartaMiejskaId;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Timestamp getWaznyOd() {
		return waznyOd;
	}

	public void setWaznyOd(Timestamp waznyOd) {
		this.waznyOd = waznyOd;
	}

	public Timestamp getWaznyDo() {
		return waznyDo;
	}

	public void setWaznyDo(Timestamp waznyDo) {
		this.waznyDo = waznyDo;
	}

	public Short getAktywna() {
		return aktywna;
	}

	public void setAktywna(Short aktywna) {
		this.aktywna = aktywna;
	}

	public Uzytkownik getUzytkownik() {
		return uzytkownik;
	}

	public void setUzytkownik(Uzytkownik uzytkownik) {
		this.uzytkownik = uzytkownik;
	}

	public Ulga getUlga() {
		return ulga;
	}

	public void setUlga(Ulga ulga) {
		this.ulga = ulga;
	}

	public Strefa getStrefa() {
		return strefa;
	}

	public void setStrefa(Strefa strefa) {
		this.strefa = strefa;
	}

	public TypKartaMiejska getTypKartaMiejska() {
		return typKartaMiejska;
	}

	public void setTypKartaMiejska(TypKartaMiejska typKartaMiejska) {
		this.typKartaMiejska = typKartaMiejska;
	}
}
