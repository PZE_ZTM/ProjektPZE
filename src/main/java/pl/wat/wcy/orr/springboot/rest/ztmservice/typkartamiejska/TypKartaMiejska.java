package pl.wat.wcy.orr.springboot.rest.ztmservice.typkartamiejska;

import pl.wat.wcy.orr.springboot.rest.ztmservice.kartamiejska.KartaMiejska;

import javax.persistence.*;
import java.util.List;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class TypKartaMiejska {
	@Id
	@GeneratedValue
	@Column(name="typkartamiejskaid")
	private Long typKartaMiejskaId;

	@Column(name="nazwa")
	private String nazwa;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="typkartamiejskaid")
	private List<KartaMiejska> kartaMiejska;

	public TypKartaMiejska() {
		super();
	}

	public TypKartaMiejska(Long typKartaMiejskaId, String nazwa, List <KartaMiejska> kartaMiejska){
		this.typKartaMiejskaId = typKartaMiejskaId;
		this.nazwa=nazwa;
		this.kartaMiejska = kartaMiejska;
	}

	public Long getTypKartaMiejskaId() {
		return typKartaMiejskaId;
	}

	public void setTypKartaMiejskaId(Long typKartaMiejskaId) {
		this.typKartaMiejskaId = typKartaMiejskaId;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public List <KartaMiejska> getKartaMiejska() {
		return kartaMiejska;
	}

	public void setKartaMiejska(List <KartaMiejska> kartaMiejska) {
		this.kartaMiejska = kartaMiejska;
	}
}
