package pl.wat.wcy.orr.springboot.rest.ztmservice.mandat;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class MandatNotFoundException extends RuntimeException {

	public MandatNotFoundException(String exception) {
		super(exception);
	}

}
