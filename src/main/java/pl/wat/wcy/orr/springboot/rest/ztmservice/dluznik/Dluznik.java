package pl.wat.wcy.orr.springboot.rest.ztmservice.dluznik;

import pl.wat.wcy.orr.springboot.rest.ztmservice.adres.Adres;
import pl.wat.wcy.orr.springboot.rest.ztmservice.mandat.Mandat;

import javax.persistence.*;
import java.util.List;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class Dluznik {
	@Id
	@GeneratedValue
	@Column(name="dluznikid" )
	private Long dluznikId;

	@Column(name="imie")
	private String	imie;

	@Column(name="nazwisko")
	private	String nazwisko;

	@Column(name="email")
	private	String email;

	@Column(name="haslo")
	private	String haslo;

	@Column(name="pesel")
	private	String pesel;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "mandatid")
	private List<Mandat> mandat;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "dluznikid" ,insertable=false ,updatable=false)
	private Adres adres;


	public Dluznik() {
		super();
	}

	public Dluznik(Long dluznikId, String imie, String nazwisko, String email, String haslo, String pesel, List <Mandat> mandat, Adres adres){

		this.dluznikId = dluznikId;
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.email = email;
		this.haslo = haslo;
		this.pesel = pesel;
		this.mandat = mandat;
		this.adres=adres;
	}

	public Long getDluznikId() {
		return dluznikId;
	}

	public void setDluznikId(Long dluznikId) {
		this.dluznikId = dluznikId;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHaslo() {
		return haslo;
	}

	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public List <Mandat> getMandat() {
		return mandat;
	}

	public void setMandat(List <Mandat> mandat) {
		this.mandat = mandat;
	}
}
