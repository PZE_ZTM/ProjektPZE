package pl.wat.wcy.orr.springboot.rest.ztmservice.dluznik;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class DluznikNotFoundException extends RuntimeException {

	public DluznikNotFoundException(String exception) {
		super(exception);
	}

}
