package pl.wat.wcy.orr.springboot.rest.ztmservice.person;

public class PersonNotFoundException extends RuntimeException {

	public PersonNotFoundException(String exception) {
		super(exception);
	}

}
