package pl.wat.wcy.orr.springboot.rest.ztmservice.kartamiejska;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class KartaMiejskaNotFoundException extends RuntimeException {

	public KartaMiejskaNotFoundException(String exception) {
		super(exception);
	}

}
