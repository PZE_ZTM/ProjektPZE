package pl.wat.wcy.orr.springboot.rest.ztmservice.strefa;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class StrefaNotFoundException extends RuntimeException {

	public StrefaNotFoundException(String exception) {
		super(exception);
	}

}
