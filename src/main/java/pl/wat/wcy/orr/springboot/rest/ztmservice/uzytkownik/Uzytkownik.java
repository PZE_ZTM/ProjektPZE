package pl.wat.wcy.orr.springboot.rest.ztmservice.uzytkownik;

import pl.wat.wcy.orr.springboot.rest.ztmservice.bilet.Bilet;
import pl.wat.wcy.orr.springboot.rest.ztmservice.kartamiejska.KartaMiejska;
import pl.wat.wcy.orr.springboot.rest.ztmservice.uprawnienia.Uprawnienia;

import javax.persistence.*;
import java.util.List;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class Uzytkownik {
	@Id
	@GeneratedValue
	@Column(name="uzytkownikid")
	private Long uzytkownikId;

	@Column(name="imie")
	private String	imie;

	@Column(name="nazwisko")
	private	String nazwisko;

	@Column(name="email")
	private	String email;

	@Column(name="haslo")
	private	String haslo;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="uzytkownikid")
	private List<Bilet> bilet;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="uzytkownikid")
	private List<KartaMiejska> kartaMiejska;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "uprawnieniaid")
	private Uprawnienia uprawnienia;



	public Uzytkownik() {
		super();
	}

	public Uzytkownik(Long uzytkownikId, String imie, String nazwisko, String email, String haslo, List <Bilet> bilet, List <KartaMiejska> kartaMiejska, Uprawnienia uprawnienia){

		this.uzytkownikId = uzytkownikId;

		this.imie = imie;
		this.nazwisko = nazwisko;
		this.email = email;
		this.haslo = haslo;
		this.bilet = bilet;
		this.kartaMiejska = kartaMiejska;
		this.uprawnienia = uprawnienia;
	}

	public Long getUzytkownikId() {
		return uzytkownikId;
	}

	public void setUzytkownikId(Long uzytkownikId) {
		this.uzytkownikId = uzytkownikId;
	}


	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHaslo() {
		return haslo;
	}

	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}


	public Uprawnienia getUprawnienia() {
		return uprawnienia;
	}

	public void setUprawnienia(Uprawnienia uprawnienia) {
		this.uprawnienia = uprawnienia;
	}

	public List <KartaMiejska> getKartaMiejska() {
		return kartaMiejska;
	}

	public void setKartaMiejska(List <KartaMiejska> kartaMiejska) {
		this.kartaMiejska = kartaMiejska;
	}

	public List <Bilet> getBilet() {
		return bilet;
	}

	public void setBilet(List <Bilet> bilet) {
		this.bilet = bilet;
	}
}
