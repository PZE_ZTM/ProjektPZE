package pl.wat.wcy.orr.springboot.rest.ztmservice.uprawnienia;

import pl.wat.wcy.orr.springboot.rest.ztmservice.uzytkownik.Uzytkownik;

import javax.persistence.*;
import java.util.List;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class Uprawnienia {
	@Id
	@GeneratedValue
	@Column(name="uprawnieniaid")
	private Long uprawnieniaId;

	@Column(name="nazwa")
	private String nazwa;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="uprawnieniaid")
	private List<Uzytkownik> uzytkownik;

	public Uprawnienia() {
		super();
	}

	public Uprawnienia(Long uprawnieniaId, String nazwa, List <Uzytkownik> uzytkownik){
		this.uprawnieniaId = uprawnieniaId;
		this.nazwa=nazwa;
		this.uzytkownik = uzytkownik;
	}

	public Long getUprawnieniaId() {
		return uprawnieniaId;
	}

	public void setUprawnieniaId(Long uprawnieniaId) {
		this.uprawnieniaId = uprawnieniaId;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public List <Uzytkownik> getUzytkownik() {
		return uzytkownik;
	}

	public void setUzytkownik(List <Uzytkownik> uzytkownik) {
		this.uzytkownik = uzytkownik;
	}
}
