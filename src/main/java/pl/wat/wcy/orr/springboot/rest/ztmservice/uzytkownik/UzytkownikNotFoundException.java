package pl.wat.wcy.orr.springboot.rest.ztmservice.uzytkownik;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class UzytkownikNotFoundException extends RuntimeException {

	public UzytkownikNotFoundException(String exception) {
		super(exception);
	}

}
