package pl.wat.wcy.orr.springboot.rest.ztmservice.mandat;

import pl.wat.wcy.orr.springboot.rest.ztmservice.dluznik.Dluznik;
import pl.wat.wcy.orr.springboot.rest.ztmservice.kontroler.Kontroler;
import pl.wat.wcy.orr.springboot.rest.ztmservice.typmandatu.TypMandatu;

import javax.persistence.*;
import java.sql.Timestamp;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class Mandat {
	@Id
	@GeneratedValue
	@Column(name = "mandatid")
	private Long mandatId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "typId")
	private TypMandatu typMandatu;

	@Column(name = "terminPlatnosci")
	private Timestamp terminPlatnosci;

	@Column(name = "dataWystawienia")
	private Timestamp dataWystawienia;

	@Column(name = "czyZaplacony")
	private Short czyZaplacony;

	@Column(name = "numerKonta")
	private String numerKonta;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "dluznikid")
	private Dluznik dluznik;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "kontrolerId")
	private Kontroler kontroler;


	public Mandat() {
		super();
	}

	public Mandat(Long mandatId, TypMandatu typMandatu, Timestamp terminPlatnosci, Timestamp dataWystawienia, Short czyZaplacony, String numerKonta, Dluznik dluznik, Kontroler kontroler){

		this.mandatId = mandatId;
		this.typMandatu = typMandatu;
		this.terminPlatnosci = terminPlatnosci;
		this.dataWystawienia = dataWystawienia;
		this.czyZaplacony = czyZaplacony;
		this.numerKonta = numerKonta;
		this.dluznik = dluznik;
		this.kontroler = kontroler;
	}


	public Long getMandatId() {
		return mandatId;
	}

	public void setMandatId(Long mandatId) {
		this.mandatId = mandatId;
	}


	public Timestamp getTerminPlatnosci() {
		return terminPlatnosci;
	}

	public void setTerminPlatnosci(Timestamp terminPlatnosci) {
		this.terminPlatnosci = terminPlatnosci;
	}

	public Timestamp getDataWystawienia() {
		return dataWystawienia;
	}

	public void setDataWystawienia(Timestamp dataWystawienia) {
		this.dataWystawienia = dataWystawienia;
	}

	public Short getCzyZaplacony() {
		return czyZaplacony;
	}

	public void setCzyZaplacony(Short czyZaplacony) {
		this.czyZaplacony = czyZaplacony;
	}

	public String getNumerKonta() {
		return numerKonta;
	}

	public void setNumerKonta(String numerKonta) {
		this.numerKonta = numerKonta;
	}

	public Kontroler getKontroler() {
		return kontroler;
	}

	public void setKontroler(Kontroler kontroler) {
		this.kontroler = kontroler;
	}

	public Dluznik getDluznik() {
		return dluznik;
	}

	public void setDluznik(Dluznik dluznik) {
		this.dluznik = dluznik;
	}

	public TypMandatu getTypMandatu() {
		return typMandatu;
	}

	public void setTypMandatu(TypMandatu typMandatu) {
		this.typMandatu = typMandatu;
	}
}
