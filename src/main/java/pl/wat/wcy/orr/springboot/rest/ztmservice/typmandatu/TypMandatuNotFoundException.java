package pl.wat.wcy.orr.springboot.rest.ztmservice.typmandatu;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class TypMandatuNotFoundException extends RuntimeException {

	public TypMandatuNotFoundException(String exception) {
		super(exception);
	}

}
