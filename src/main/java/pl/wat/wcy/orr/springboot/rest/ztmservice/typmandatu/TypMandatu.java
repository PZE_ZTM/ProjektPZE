package pl.wat.wcy.orr.springboot.rest.ztmservice.typmandatu;

import pl.wat.wcy.orr.springboot.rest.ztmservice.mandat.Mandat;

import javax.persistence.*;
import java.util.List;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity(name = "typ_mandatu")
public class TypMandatu {
	@Id
	@GeneratedValue
  @Column(name="typId")
	private Long typId;

  @Column(name="kwota")
	private Integer kwota;

  @Column(name="nazwaTyp")
	private String nazwaTyp;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinColumn(name="typId")
  private List<Mandat> mandat;

	public TypMandatu() {
		super();
	}

	public TypMandatu(Long typId, Integer kwota, String nazwaTyp){

        this.typId = typId;
        this.kwota = kwota;
        this.nazwaTyp = nazwaTyp;
    }

    public String getNazwaTyp() {
        return nazwaTyp;
    }

    public void setNazwaTyp(String nazwaTyp) {
        this.nazwaTyp = nazwaTyp;
    }

    public Integer getKwota() {
        return kwota;
    }

    public void setKwota(Integer kwota) {
        this.kwota = kwota;
    }

    public Long getTypId() {
        return typId;
    }

    public void setTypId(Long typId) {
        this.typId = typId;
    }

    public List <Mandat> getMandat() {
        return mandat;
    }

    public void setMandat(List <Mandat> mandat) {
        this.mandat = mandat;
    }
}
