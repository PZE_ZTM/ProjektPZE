package pl.wat.wcy.orr.springboot.rest.ztmservice.bilet;

import pl.wat.wcy.orr.springboot.rest.ztmservice.strefa.Strefa;
import pl.wat.wcy.orr.springboot.rest.ztmservice.typbiletu.TypBiletu;
import pl.wat.wcy.orr.springboot.rest.ztmservice.ulga.Ulga;
import pl.wat.wcy.orr.springboot.rest.ztmservice.uzytkownik.Uzytkownik;

import javax.persistence.*;
import java.sql.Timestamp;


/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

@Entity
public class Bilet {
	@Id
	@GeneratedValue
	@Column(name="biletid")
	private Long biletId;

	@Column(name="waznyOd")
	private Timestamp waznyOd;

	@Column(name="waznyDo")
	private Timestamp waznyDo;

	@Column(name="czyZaplacony")
	private Short czyZaplacony;

	@Column(name="numerKonta")
	private String numerKonta;

	@Column(name="kwota")
	private Integer kwota;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "typid")
	private TypBiletu typBiletu;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ulgaid")
	private Ulga ulga;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "strefaId")
	private Strefa strefa;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "uzytkownikid")
	private Uzytkownik uzytkownik;


	public Bilet() {
		super();
	}

	public Bilet(Long biletId, Timestamp waznyOd, Timestamp waznyDo, Short czyZaplacony, String numerKonta, Integer kwota, TypBiletu typBiletu, Ulga ulga, Strefa strefa, Uzytkownik uzytkownik){

		this.biletId = biletId;
		this.waznyOd = waznyOd;
		this.waznyDo = waznyDo;
		this.czyZaplacony = czyZaplacony;
		this.numerKonta = numerKonta;
		this.kwota = kwota;
		this.typBiletu = typBiletu;
		this.ulga = ulga;
		this.strefa = strefa;
		this.uzytkownik = uzytkownik;
	}


	public Long getBiletId() {
		return biletId;
	}

	public void setBiletId(Long biletId) {
		this.biletId = biletId;
	}

	public Timestamp getWaznyOd() {
		return waznyOd;
	}

	public void setWaznyOd(Timestamp waznyOd) {
		this.waznyOd = waznyOd;
	}

	public Timestamp getWaznyDo() {
		return waznyDo;
	}

	public void setWaznyDo(Timestamp waznyDo) {
		this.waznyDo = waznyDo;
	}

	public Short getCzyZaplacony() {
		return czyZaplacony;
	}

	public void setCzyZaplacony(Short czyZaplacony) {
		this.czyZaplacony = czyZaplacony;
	}

	public String getNumerKonta() {
		return numerKonta;
	}

	public void setNumerKonta(String numerKonta) {
		this.numerKonta = numerKonta;
	}

	public Integer getKwota() {
		return kwota;
	}

	public void setKwota(Integer kwota) {
		this.kwota = kwota;
	}

	public Uzytkownik getUzytkownik() {
		return uzytkownik;
	}

	public void setUzytkownik(Uzytkownik uzytkownik) {
		this.uzytkownik = uzytkownik;
	}

	public Strefa getStrefa() {
		return strefa;
	}

	public void setStrefa(Strefa strefa) {
		this.strefa = strefa;
	}

	public Ulga getUlga() {
		return ulga;
	}

	public void setUlga(Ulga ulga) {
		this.ulga = ulga;
	}

	public TypBiletu getTypBiletu() {
		return typBiletu;
	}

	public void setTypBiletu(TypBiletu typBiletu) {
		this.typBiletu = typBiletu;
	}
}
