package pl.wat.wcy.orr.springboot.rest.ztmservice.uprawnienia;

/**
 * @author Daniel Wojtkowski
 * @version 1.0
 * @since 2018-05-20
 * */

public class UprawnieniaNotFoundException extends RuntimeException {

	public UprawnieniaNotFoundException(String exception) {
		super(exception);
	}

}
